//
//  HomeViewController.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import MobileCoreServices
class HomeViewController: UIViewController {
    var rightUsers = [UserModel]()
    var leftUsers = [UserModel]()
    @IBOutlet weak var rightTableView: UITableView!
    
    @IBOutlet weak var leftTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        rightTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        leftTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        rightTableView.delegate = self
        rightTableView.dataSource = self
        leftTableView.delegate = self
        leftTableView.dataSource = self
        leftTableView.dragDelegate = self
        leftTableView.dropDelegate = self
        rightTableView.dragDelegate = self
        rightTableView.dropDelegate = self

        leftTableView.dragInteractionEnabled = true
        rightTableView.dragInteractionEnabled = true
        
        rightUsers.append(UserModel(username: "Harry Potter", avatarImage: "https://timedotcom.files.wordpress.com/2014/07/301386_full1.jpg"))
        rightUsers.append(UserModel(username: "Các Mác", avatarImage: "http://lyluanchinhtri.vn/home/media/k2/items/cache/270346aaee566bd7e522cfe834c2438a_L.jpg"))
        leftUsers.append(UserModel(username: "Lenin", avatarImage: "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTIwNjA4NjMzODgyNTEwODYw/vladimir-lenin-9379007-1-402.jpg"))
        leftUsers.append(UserModel(username: "Hiệp sĩ trong tay có kiếm", avatarImage: "https://www.hiepsibaotap.com/wp-content/uploads/2018/11/6_green_ranger-532x405-1.jpg"))
      
    }


    

}
extension HomeViewController: UITableViewDelegate{
    
}
extension HomeViewController: UITableViewDragDelegate{
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = tableView == leftTableView ? leftUsers[indexPath.row] : rightUsers[indexPath.row]
        let itemProvider = NSItemProvider(object: item)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        return [dragItem]
    }
}
extension HomeViewController: UITableViewDropDelegate{
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {

        let destinationIndexPath: IndexPath
        //Get destination indexpath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            // At the last row of tableview
            let section = tableView.numberOfSections - 1
            let row = tableView.numberOfRows(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
        coordinator.session.loadObjects(ofClass: UserModel.self){
            items in
            guard let items = items as? [UserModel] else {
                return
            }
            var indexPaths = [IndexPath]()

            // loop over all the object we received
            for (index, item) in items.enumerated() {
                // create an index path for this new row, moving it down depending on how many we've already inserted
                let indexPath = IndexPath(row: destinationIndexPath.row + index, section: destinationIndexPath.section)

                // insert the copy into the correct array
                if tableView == self.leftTableView {
                    self.leftUsers.insert(item, at: indexPath.row)
                } else {
                    self.rightUsers.insert(item, at: indexPath.row)
                }

                // keep track of this new row
                indexPaths.append(indexPath)
            }
            tableView.insertRows(at: indexPaths, with: .automatic)
        }


    }


}
extension HomeViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == rightTableView{
            return rightUsers.count
        } else if tableView == leftTableView {
            return leftUsers.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemCell.reuseIdentifier, for: indexPath) as! ItemCell
        if tableView == rightTableView {
            cell.user = rightUsers[indexPath.row]
        } else {
            cell.user = leftUsers[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width / 3
    }
    
    
}
