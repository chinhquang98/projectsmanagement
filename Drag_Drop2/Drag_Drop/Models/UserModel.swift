//
//  UserModel.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import Foundation


class UserModel {
    
    var username: String?
    var avatarImage: String?
    init(username: String, avatarImage: String){
        self.username = username
        self.avatarImage = avatarImage
    }
}
