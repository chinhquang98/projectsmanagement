//
//  UIView_extensions.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/9/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
extension UIView{
    func createSnapShot() -> UIView {
        // Get cell layer as ImageView
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        // Create cell snapshot
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5, height: 0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}
