//
//  HomeViewController.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/6/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
import MobileCoreServices
class DragDropTableView: UITableView {
    var users:[UserModel] = [UserModel]()
}
struct My {
    static var cellSnapshot : UIView? = nil
}
struct Path {
    static var initialIndexPath : IndexPath? = nil
}
struct Table{
    static var tag : Int?
}
class HomeViewController: UIViewController {
    
    @IBOutlet weak var rightTableView: DragDropTableView!
    
    @IBOutlet weak var leftTableView: DragDropTableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        rightTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        leftTableView.register(UINib(nibName: ItemCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ItemCell.reuseIdentifier)
        rightTableView.delegate = self
        rightTableView.dataSource = self
        leftTableView.delegate = self
        leftTableView.dataSource = self
        rightTableView.tag = 1
        leftTableView.tag = 2
        loadData()
        let rightLongPressRecognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(didLongPressCell(recognizer:)))
        let leftLongPressRecognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(didLongPressCell(recognizer:)))
        rightTableView.addGestureRecognizer(rightLongPressRecognizer)
        leftTableView.addGestureRecognizer(leftLongPressRecognizer)
        
    }
    func loadData() {
        rightTableView.users.append(UserModel(username: "Harry Potter", avatarImage: "https://timedotcom.files.wordpress.com/2014/07/301386_full1.jpg"))
        rightTableView.users.append(UserModel(username: "Các Mác", avatarImage: "http://lyluanchinhtri.vn/home/media/k2/items/cache/270346aaee566bd7e522cfe834c2438a_L.jpg"))
        
        leftTableView.users.append(UserModel(username: "Lenin", avatarImage: "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_auto:good%2Cw_300/MTIwNjA4NjMzODgyNTEwODYw/vladimir-lenin-9379007-1-402.jpg"))
        leftTableView.users.append(UserModel(username: "Hiệp sĩ trong tay có kiếm", avatarImage: "https://www.hiepsibaotap.com/wp-content/uploads/2018/11/6_green_ranger-532x405-1.jpg"))
        
    }
    @objc func didLongPressCell (recognizer: UILongPressGestureRecognizer) {
        let cellPP: UIView = recognizer.view!
        /* Get tableview tag and indexpath of the cell whose recognizer is handled
         tag = 1 : Right tableview
         tag = 2 : Left tableview
         */
        Table.tag = cellPP.tag
        if Table.tag == 1 {
            didDrag(fromTableView: rightTableView, toTableView: leftTableView, recognizer: recognizer)
        } else if Table.tag == 2{
            didDrag(fromTableView: leftTableView, toTableView: rightTableView, recognizer: recognizer)
        }
    }
}
