//
//  HomeViewController_extension.swift
//  Drag_Drop
//
//  Created by chinh.tq on 8/9/19.
//  Copyright © 2019 chinh.tq. All rights reserved.
//

import UIKit
extension HomeViewController{
    //MARK: - Calculate the area of a frame
    func calculateArea(of intersection : CGRect) -> CGFloat {
        return intersection.width * intersection.height
    }
    //MARK: - Get coordinate of the cell's center point at specific indexPath of TableView
    func getCoordinateOfCell(_ tableView: UITableView, at indexPath : IndexPath) -> CGPoint {
        let cellContentView = tableView.cellForRow(at: indexPath)?.contentView
        let rect = cellContentView!.convert(cellContentView!.frame, to: self.view) // pass toView nil if you want to convert rect relative to window
        let point = CGPoint(x: rect.origin.x + rect.size.width / 2, y: rect.origin.y + rect.size.height / 2)
        return point
    }
    //MARK: - Snap shot fade in animation
    func snapshotCreatedAnimation() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            My.cellSnapshot!.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            My.cellSnapshot!.alpha = 0.98
            
        }, completion: nil)
    }
    //MARK: - Snap shot fade out animation
    func onCompleteDropAnimation(cellPoint : CGPoint!) {
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            My.cellSnapshot!.center = cellPoint
            My.cellSnapshot!.transform = CGAffineTransform.identity
            My.cellSnapshot!.alpha = 0.0
            
            My.cellSnapshot!.removeFromSuperview()
        }, completion: { (finished) -> Void in
            if finished {
                Path.initialIndexPath = nil
                My.cellSnapshot = nil
            }
        })
    }
    //MARK : - Perform once Long press gesture recognizer event is handled
    func didDrag(fromTableView sourceTableView : DragDropTableView, toTableView destTableView: DragDropTableView, recognizer : UILongPressGestureRecognizer){
        let indexPath = sourceTableView.indexPathForRow(at: recognizer.location(in: sourceTableView))
        switch recognizer.state {
        case .began:
            let cellView = sourceTableView.cellForRow(at: indexPath!) as! ItemCell
            // Set initial indexpath
            Path.initialIndexPath = indexPath
            //Create snapshot of the cell
            My.cellSnapshot = cellView.createSnapShot()
            My.cellSnapshot!.alpha = 0.0
            My.cellSnapshot!.center = recognizer.location(in: self.view)
            view.addSubview(My.cellSnapshot!)
            //Perform fade-in animation
            snapshotCreatedAnimation()
        case .changed:
            guard My.cellSnapshot != nil else {
                return
            }
            My.cellSnapshot?.center = recognizer.location(in: view)
        default:
            let destIntersection = My.cellSnapshot?.frame.intersection(destTableView.frame)
            let sourceIntersection = My.cellSnapshot?.frame.intersection(sourceTableView.frame)
            let areaDest = calculateArea(of: destIntersection!)
            let areaSource = calculateArea(of: sourceIntersection!)
            
            var destinationIndexPath = IndexPath(row: destTableView.users.count - 1 , section: 0)
            guard let dest_index = destTableView.indexPathForRow(at: recognizer.location(in: destTableView)) else {
                guard areaDest > areaSource else {
                    print("At source table")
                    guard (Path.initialIndexPath != nil) else {
                        return
                    }
                    let cell = sourceTableView.cellForRow(at: Path.initialIndexPath!)
                    let cellPoint = cell?.center
                    onCompleteDropAnimation(cellPoint: cellPoint!)
                    return
                }
                let firstIndexPath = IndexPath(row: 0, section: 0)
                let point = getCoordinateOfCell(destTableView, at: firstIndexPath)
                if (My.cellSnapshot?.center.y)! <= point.y{
                    print("At first row of dest table")
                    destTableView.users.insert(sourceTableView.users[(Path.initialIndexPath?.row)!], at: 0)
                    destinationIndexPath = IndexPath(row: 0 , section: 0)
                    destTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                } else {
                    print("At last row of dest table")
                    destTableView.users.append(sourceTableView.users[(Path.initialIndexPath?.row)!])
                    destinationIndexPath = IndexPath(row: destTableView.users.count - 1 , section: 0)
                    destTableView.insertRows(at: [destinationIndexPath], with: .automatic)
                }
                let cellPoint = getCoordinateOfCell(destTableView, at: destinationIndexPath)
                onCompleteDropAnimation(cellPoint: cellPoint)
                return
            
            }
            print("At \(dest_index.row)th of dest  ")
            destinationIndexPath = dest_index
            let point = getCoordinateOfCell(destTableView, at: dest_index)
            let currectY  = My.cellSnapshot?.center.y
            if currectY! - point.y >= 5.0{
                destinationIndexPath = IndexPath(row: destinationIndexPath.row + 1, section: 0)
            }
            destTableView.users.insert(sourceTableView.users[(Path.initialIndexPath?.row)!], at: destinationIndexPath.row)
            destTableView.insertRows(at: [destinationIndexPath], with: .automatic)
            let indexPathArray = destTableView.indexPathsForVisibleRows
            if destinationIndexPath.row > (indexPathArray?.last!.row)!{
                destinationIndexPath = (indexPathArray?.last)!
            }
            let cellPoint = getCoordinateOfCell(destTableView, at: destinationIndexPath)
            onCompleteDropAnimation(cellPoint: cellPoint)
        }
    }
}
// UITableViewDelegate methods
extension HomeViewController : UITableViewDelegate{
    
}
// UITableViewDatasource methods
extension HomeViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rightTableView == tableView ? rightTableView.users.count : leftTableView.users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemCell.reuseIdentifier, for: indexPath) as! ItemCell
        cell.user = tableView == rightTableView ? rightTableView.users[indexPath.row] : leftTableView.users[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width / 3
    }
}

